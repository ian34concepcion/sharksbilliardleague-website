<?php if ( is_user_logged_in() ) : ?>
		
	<?php get_header();?>

	<?php if(have_posts()) :  while(have_posts()) : the_post(); ?>
	<?php if( have_rows('arena_detail') ): ?>
	<?php while( have_rows('arena_detail') ): the_row(); ?>

	<div class="video-page-container">
		<div class="video-page-livestream">

			<!-- CONDITION IF LIVESTREAM URL IS AVAILABLE -->
			<?php if( get_sub_field('livestream_option') == 'live' && get_sub_field('livestream_url')): ?> 
			
				<div class="video-playing-wrapper">
					<div class="video-playing-iframe">
						<iframe src="<?php the_sub_field('livestream_url'); ?>" title="livestream video" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
					</div>

					<div class="video-playing-detail mt-1">
						<h3 class="uppercase"><?php the_sub_field('title'); ?></h3>
					</div>

					<div class="video-playing-description uppercase">
						<p><?php the_sub_field('players'); ?></p>
						<p><?php the_sub_field('day'); ?> | <?php the_sub_field('pool_game'); ?> | <?php the_sub_field('date'); ?></p>
						<span class="hide">200 watching now</span>
					</div>
				</div>

			<?php else: // livestream_url returned false ?>
				<div class="video-playing-description uppercase mt-0 mb-2">
					<p class="no-livestream">There is currently no livestream on this arena.</p>
				</div>
			<?php endif; // end of if livestream_url logic ?>

			<section class="previous-matches-wrap db-slider">
				<h4>PREVIOUS MATCHES</h4>

				<div class="previous-matches-list previous-matches-tiles">

					<a href="/video-page.html" class="previous-match-item">
						<div class="previous-match-card">
							<div class="previous-match-thumbnail">
								<img src="<?php bloginfo('template_directory'); ?>/assets/db-assets/sld1.jpg" alt="Previous match thumbnail">
							</div>
							<div class="previous-match-details card-details">
								<div class="left-desc">
									<p>Ultimate shoutout</p>
								</div>

								<div class="right-desc">
									<div class="views">5.5k <i class="icon icon-eye"></i></div>
								</div>
							</div>
						</div>
					</a>

					<a href="/video-page.html" class="previous-match-item">
						<div class="previous-match-card">
							<div class="previous-match-thumbnail">
								<img src="<?php bloginfo('template_directory'); ?>/assets/db-assets/sld2.jpg" alt="Previous match thumbnail">
							</div>
							<div class="previous-match-details card-details">
								<div class="left-desc">
									<p>THREE KINGS</p>
								</div>

								<div class="right-desc">
									<div class="views">5.5k <i class="icon icon-eye"></i></div>
								</div>
							</div>
						</div>
					</a>

					<a href="/video-page.html" class="previous-match-item">
						<div class="previous-match-card">
							<div class="previous-match-thumbnail">
								<img src="<?php bloginfo('template_directory'); ?>/assets/db-assets/sld3.jpg" alt="Previous match thumbnail">
							</div>
							<div class="previous-match-details card-details">
								<div class="left-desc">
									<p>Morra vs Gonzales</p>
								</div>

								<div class="right-desc">
									<div class="views">5.5k <i class="icon icon-eye"></i></div>
								</div>
							</div>
						</div>
					</a>

					<a href="/video-page.html" class="previous-match-item">
						<div class="previous-match-card">
							<div class="previous-match-thumbnail">
								<img src="<?php bloginfo('template_directory'); ?>/assets/db-assets/sld4.jpg" alt="Previous match thumbnail">
							</div>
							<div class="previous-match-details card-details">
								<div class="left-desc">
									<p>TOP GUNS</p>
								</div>

								<div class="right-desc">
									<div class="views">5.5k <i class="icon icon-eye"></i></div>
								</div>
							</div>
						</div>
					</a>

					<a href="/video-page.html" class="previous-match-item">
						<div class="previous-match-card">
							<div class="previous-match-thumbnail">
								<img src="<?php bloginfo('template_directory'); ?>/assets/db-assets/sld4.jpg" alt="Previous match thumbnail">
							</div>
							<div class="previous-match-details card-details">
								<div class="left-desc">
									<p>TOP GUNS</p>
								</div>

								<div class="right-desc">
									<div class="views">5.5k <i class="icon icon-eye"></i></div>
								</div>
							</div>
						</div>
					</a>

					<a href="/video-page.html" class="previous-match-item">
						<div class="previous-match-card">
							<div class="previous-match-thumbnail">
								<img src="<?php bloginfo('template_directory'); ?>/assets/db-assets/sld4.jpg" alt="Previous match thumbnail">
							</div>
							<div class="previous-match-details card-details">
								<div class="left-desc">
									<p>TOP GUNS</p>
								</div>

								<div class="right-desc">
									<div class="views">5.5k <i class="icon icon-eye"></i></div>
								</div>
							</div>
						</div>
					</a>

				</div>
			</section>
		</div>

		<div class="aside-video-details">
			<div class="arenas-slider hide-mobile">
				<?php previous_post_link(); ?>   
				<div class="arenas-slider-list">
					<a href="<?php the_permalink() ?>" class="arenas-slider-item">
						<img src="<?php the_sub_field('arena_logo'); ?>" alt="Arena logo">
					</a>
				</div>
				<?php next_post_link(); ?>
			</div>
			<div class="game-schedule-details">
				<ul>
					<li>
						<p>Game Schedule</p>
						<h5>May 19, 2023</h5>
					</li>

					<li>
						<h4>Three kings</h4>
						<p>3 Man Battle</p>
						<p>10 Ball | Race to 25</p>
					</li>

					<li class="live-now">
						<h4>THE LEGENDS I</h4>
						<div class="live-badge">
							Live <i class="icon icon-circle"></i>
						</div>
						<p>STRICKLAND VS BUSTAMANTE</p>
						<p>DAY 1 of 3 | 10 BALL | RACE TO 25</p>
					</li>

					<li>
						<h4>10 MAN SHOWDOWN</h4>
						<p>10 MAN BATTLE</p>
						<p>10 BALL | RACE TO 9</p>
					</li>
				</ul>
			</div>

			<?php get_template_part('includes/section', 'advertisement'); ?>
		</div>
	</div>

	<?php endwhile; ?>
	<?php endif; ?>

	<?php endwhile; ?>

	<?php else: ?>
			<p>Nothing to post</p>
	<?php endif; ?>

	<?php get_footer();?>

<?php else : ?>
	<?php wp_redirect('/log-in');  ?>
<?php endif; ?>
