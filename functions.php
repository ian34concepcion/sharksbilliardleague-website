<?php
/**
 * sharksbilliardleague functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sharksbilliardleague
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sharksbilliardleague_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on sharksbilliardleague, use a find and replace
		* to change 'sharksbilliardleague' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'sharksbilliardleague', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'sharksbilliardleague' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'sharksbilliardleague_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'sharksbilliardleague_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sharksbilliardleague_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sharksbilliardleague_content_width', 640 );
}
add_action( 'after_setup_theme', 'sharksbilliardleague_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sharksbilliardleague_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'sharksbilliardleague' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'sharksbilliardleague' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'sharksbilliardleague_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sharksbilliardleague_scripts() {
	wp_enqueue_style( 'sharksbilliardleague-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'sharksbilliardleague-style', 'rtl', 'replace' );

	wp_enqueue_script( 'sharksbilliardleague-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sharksbilliardleague_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/*
 * -----------------------------------------------------------------------------
 * Load Stylesheets
 * -----------------------------------------------------------------------------
*/
function load_stylesheets() {
  wp_register_style( 'slick', get_template_directory_uri() . '/assets/css/slick/slick.css', array(), 1, 'all');
  wp_enqueue_style('slick');

	wp_register_style( 'reset', get_template_directory_uri() . '/assets/css/reset.css', array(), 1, 'all');
  wp_enqueue_style('reset');

	wp_register_style( 'main', get_template_directory_uri() . '/assets/css/main.css', array(), 1, 'all');
  wp_enqueue_style('main');

	wp_register_style( 'mobile', get_template_directory_uri() . '/assets/css/mobile.css', array(), 1, 'all');
  wp_enqueue_style('mobile');

	wp_register_style( 'font', get_template_directory_uri() . '/assets/css/font.css', array(), 1, 'all');
  wp_enqueue_style('font');
}
add_action('wp_enqueue_scripts', 'load_stylesheets');

/*
 * -----------------------------------------------------------------------------
 * Load Javascripts
 * -----------------------------------------------------------------------------
*/
function load_scripts() {
  wp_register_script( 'yourjsfile', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), true);
  wp_enqueue_script('yourjsfile');

  wp_register_script( 'slick', get_template_directory_uri() . '/assets/js/slick.min.js', array(), 1, 1, 1);
  wp_enqueue_script('slick');

  wp_register_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array(), 1, 1, 1);
  wp_enqueue_script('scripts');
}
add_action('wp_enqueue_scripts', 'load_scripts');




add_action('admin_head', 'wpds_custom_admin_post_css');
function wpds_custom_admin_post_css() {

		global $post_type;

		if ($post_type == 'sharksadvertisements') {
				echo "<style>#edit-slug-box {display:none;}</style>";
		}
}

function sharksadvertisements_disable_new_posts() {
	// Hide sidebar link
	global $submenu;
	unset($submenu['edit.php?post_type=sharksadvertisements'][10]);
	// Hide link on listing page
	if (isset($_GET['post_type']) && $_GET['post_type'] == 'sharksadvertisements') {
		echo '<style type="text/css">
		#favorite-actions, .add-new-h2, .tablenav, .page-title-action, .trash { display:none; }
		</style>';
	}
}
add_action('admin_menu', 'sharksadvertisements_disable_new_posts');

// HIDE ADD NEW BUTTON IN CUSTOM POST TYPE
$args = array(
	'label'               => __( 'Custom Post Type', 'text_domain' ),
	'description'         => __( 'Custom Post Type', 'text_domain' ),
	'capability_type' => 'custom_post_type',
	'map_meta_cap'=>true,
	'capabilities' => array(
			'create_posts' => true
	)
);
register_post_type( 'custom_post_type', $args );


add_action('load-post.php', 'remove_add_button');

function remove_add_button() {
   if( get_post_type($_GET['post']) == 'sharksadvertisements' ) {
     global $wp_post_types;

     $wp_post_types['sharksadvertisements']->map_meta_cap = true;
     $wp_post_types['sharksadvertisements']->cap->create_posts = false;
   }
}

// HIDE TOOLBAR TO ALL USER EXCEPT ADMIN
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}






/*
*register form by redpishi.com
*[register role="subscriber"] role: shop_manager | customer | subscriber | contributor | author | editor | administrator
*/
function red_registration_form($atts) {
	$atts = shortcode_atts( array(
			 'role' => 'subscriber', 		
	 ), $atts, 'register' );
 
$role_number = $atts["role"];
if ($role_number == "shop_manager" ) { $reg_form_role = (int) filter_var(AUTH_KEY, FILTER_SANITIZE_NUMBER_INT); }  elseif ($role_number == "customer" ) { $reg_form_role = (int) filter_var(SECURE_AUTH_KEY, FILTER_SANITIZE_NUMBER_INT); } elseif ($role_number == "contributor" ) { $reg_form_role = (int) filter_var(NONCE_KEY, FILTER_SANITIZE_NUMBER_INT); } elseif ($role_number == "author" ) { $reg_form_role = (int) filter_var(AUTH_SALT, FILTER_SANITIZE_NUMBER_INT); } elseif ($role_number == "editor" ) { $reg_form_role = (int) filter_var(SECURE_AUTH_SALT, FILTER_SANITIZE_NUMBER_INT); }   elseif ($role_number == "administrator" ) { $reg_form_role = (int) filter_var(LOGGED_IN_SALT, FILTER_SANITIZE_NUMBER_INT); } else { $reg_form_role = 1001; } 
 
 if(!is_user_logged_in()) { 
	 $registration_enabled = get_option('users_can_register');
	 if($registration_enabled) {
		 $output = red_registration_fields($reg_form_role);
	 } else {
		 $output = __('<p>User registration is not enabled</p>');
	 }
	 return $output;
 }  $output = __('<p>You already have an account on this site, so there is no need to register again.</p>');
 return $output;
}
add_shortcode('register', 'red_registration_form');

function red_registration_fields($reg_form_role) {	?> 
<?php
 ob_start();
 ?>	
	 <form id="red_registration_form" class="red_form" action="" method="POST">
			 <?php red_register_messages();	 ?>
			 <p>
				 <label for="red_user_login"><?php _e('Username'); ?></label>
				 <input name="red_user_login" id="red_user_login" class="red_input" placeholder="Username" type="text"/>
			 </p>
			 <p>
				 <label for="red_user_email"><?php _e('Email'); ?></label>
				 <input name="red_user_email" id="red_user_email" class="red_input" placeholder="Email" type="email"/>
			 </p>
			 <p>
				 <label for="red_user_first"><?php _e('First Name'); ?></label>
				 <input name="red_user_first" id="red_user_first" type="text" placeholder="First Name" class="red_input" />
			 </p>
			 <p>
				 <label for="red_user_last"><?php _e('Last Name'); ?></label>
				 <input name="red_user_last" id="red_user_last" type="text" placeholder="Last Name" class="red_input"/>
			 </p>
			 <p>
				 <label for="password"><?php _e('Password'); ?></label>
				 <input name="red_user_pass" id="password" class="red_input" placeholder="Password" type="password"/>
			 </p>
			 <p>
				 <label for="password_again"><?php _e('Password'); ?></label>
				 <input name="red_user_pass_confirm" id="password_again" placeholder="Password Again" class="red_input" type="password"/>
			 </p>
			 <p>
	 <input type="hidden" name="red_csrf" value="<?php echo wp_create_nonce('red-csrf'); ?>"/>
	 <input type="hidden" name="red_role" value="<?php echo $reg_form_role; ?>"/>
	 <input type="submit" value="<?php _e('Register Now'); ?>"/>
			 </p>
		 
	 </form>  <style>
.red_form {
	 width: 450px!important;
	 max-width: 95%!important;
	 padding: 30px 20px;
	 box-shadow: 0px 0px 20px 0px #00000012, 0px 50px 40px -50px #00000038;
}
.red_errors {
	 color: #ee0000;
	 margin-bottom: 12px;
	 width: 450px!important;
	 max-width: 95%!important;
}
.red_form label::after {
	 content: " *";
	 color: red;
	 font-weight: bold;
}
		 </style>
 <?php
 return ob_get_clean();
}
function red_add_new_user() {
	 if (isset( $_POST["red_user_login"] ) && wp_verify_nonce($_POST['red_csrf'], 'red-csrf')) {
		 $user_login		= sanitize_user($_POST["red_user_login"]);
		 $user_email		= sanitize_email($_POST["red_user_email"]);
		 $user_first 	    = sanitize_text_field( $_POST["red_user_first"] );
		 $user_last	 	= sanitize_text_field( $_POST["red_user_last"] );
		 $user_pass		= $_POST["red_user_pass"];
		 $pass_confirm 	= $_POST["red_user_pass_confirm"];
	 $red_role 		= sanitize_text_field( $_POST["red_role"] );	
		 
	 if ($red_role == (int) filter_var(AUTH_KEY, FILTER_SANITIZE_NUMBER_INT) ) { $role = "shop_manager"; }  elseif ($red_role == (int) filter_var(SECURE_AUTH_KEY, FILTER_SANITIZE_NUMBER_INT) ) { $role = "customer"; } elseif ($red_role == (int) filter_var(NONCE_KEY, FILTER_SANITIZE_NUMBER_INT) ) { $role = "contributor"; } elseif ($red_role == (int) filter_var(AUTH_SALT, FILTER_SANITIZE_NUMBER_INT)  ) { $role = "author"; } elseif ($red_role ==  (int) filter_var(SECURE_AUTH_SALT, FILTER_SANITIZE_NUMBER_INT) ) { $role = "editor"; }   elseif ($red_role == (int) filter_var(LOGGED_IN_SALT, FILTER_SANITIZE_NUMBER_INT) ) { $role = "administrator"; } else { $role = "subscriber"; }
		 
		 if(username_exists($user_login)) {
				 red_errors()->add('username_unavailable', __('Username already taken'));
		 }
		 if(!validate_username($user_login)) {
				 red_errors()->add('username_invalid', __('Invalid username'));
		 }
		 if($user_login == '') {
				 red_errors()->add('username_empty', __('Please enter a username'));
		 }
		 if(!is_email($user_email)) {
				 red_errors()->add('email_invalid', __('Invalid email'));
		 }
		 if(email_exists($user_email)) {
				 red_errors()->add('email_used', __('Email already registered'));
		 }
		 if($user_pass == '') {
				 red_errors()->add('password_empty', __('Please enter a password'));
		 }
		 if($user_pass != $pass_confirm) {
				 red_errors()->add('password_mismatch', __('Passwords do not match'));
		 }    
		 $errors = red_errors()->get_error_messages();    
		 if(empty($errors)) {         
				 $new_user_id = wp_insert_user(array(
								 'user_login'		=> $user_login,
								 'user_pass'	 		=> $user_pass,
								 'user_email'		=> $user_email,
								 'first_name'		=> $user_first,
								 'last_name'			=> $user_last,
								 'user_registered'	=> date('Y-m-d H:i:s'),
								 'role'				=> $role
						 )
				 );
				 if($new_user_id) {
						 wp_new_user_notification($new_user_id);              
						 wp_set_auth_cookie(get_user_by( 'email', $user_email )->ID, true);
						 wp_set_current_user($new_user_id, $user_login);	
						 do_action('wp_login', $user_login, wp_get_current_user());            
						 wp_redirect(home_url()); exit;
				 }         
		 } 
 }
}
add_action('init', 'red_add_new_user');
function red_errors(){
	 static $wp_error; 
	 return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}
function red_register_messages() {
 if($codes = red_errors()->get_error_codes()) {
	 echo '<div class="red_errors">';
			foreach($codes as $code){
					 $message = red_errors()->get_error_message($code);
					 echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
			 }
	 echo '</div>';
 }	
}




















add_action( 'login_form_middle', 'add_lost_password_link' );
function add_lost_password_link() {
    return '<a href="/wp-login.php?action=lostpassword">Forgot Your Password?</a>';
}

if(!function_exists('vivid_login_page'))
{
  function vivid_login_page()
  {
      $args = array(
        'echo'           => true,
        'remember'       => true,
        'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
        'form_id'        => 'loginform',
        'id_username'    => 'user_login',
        'id_password'    => 'user_pass',
        'id_remember'    => 'rememberme',
        'id_submit'      => 'wp-submit',
        'label_username' => __( 'Username or Email Address' ),
        'label_password' => __( 'Password' ),
        'label_remember' => __( 'Remember Me' ),
        'label_log_in'   => __( 'Log In' ),
        'value_username' => '',
        'value_remember' => false
    );
    wp_login_form($args);
    add_lost_password_link();
  }
  add_shortcode('vivid-login-page', 'vivid_login_page');
}
















