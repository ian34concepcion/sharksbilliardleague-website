<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sharksbilliardleague
 */

?>

	<div id="stop" class="scrollTop">
		<span><a href="">Top</a></span>
	</div>

</div> <!-- main-content -->

<?php wp_footer(); ?>

</body>
</html>
