<?php if ( is_user_logged_in() ) : ?>

  <?php
  /* Template Name: Matches */
  get_header();
  ?>

  <?php get_template_part('includes/section', 'arena-one'); ?>
  <?php get_template_part('includes/section', 'arena-two'); ?>
  <?php get_template_part('includes/section', 'arena-three'); ?>
  <?php get_template_part('includes/section', 'arena-four'); ?>

  <?php get_template_part('includes/section', 'footer'); ?>

  <?php get_footer(); ?>

<?php else : ?>
	<?php wp_redirect('/log-in');  ?>
<?php endif; ?>
